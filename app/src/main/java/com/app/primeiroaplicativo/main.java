package com.app.primeiroaplicativo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.media.MediaPlayer;

public class main extends Activity {
    MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mediaPlayer = MediaPlayer.create(this, R.raw.song);
    }

    public void tocar(View view) {
        if (mediaPlayer.isPlaying())
            mediaPlayer.seekTo(0);
        else{
            mediaPlayer.start();
        }
    }
}
